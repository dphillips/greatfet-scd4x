#!/usr/bin/env python

import sensor
import driver
import greatfet
import time


class Scd41GreatFET(driver.Scd41):
    def __init__(self, address):
        self._gf = greatfet.GreatFET()
        self._address = address

    def send_bytes(self, data):
        self._gf.i2c.write(self._address, data)

    def recv_bytes(self, bytes_count):
        return self._gf.i2c.read(self._address, bytes_count)


if __name__ == "__main__":
    scd = sensor.Sensor(Scd41GreatFET(0x62))
    serial = scd.read_serial_number()
    print(f"Serial: {serial}")

    scd.measure_single_shot()
    print("Started single shot")
    time.sleep(5)
    measurement = scd.read_measurement()
    print(f"Measurement: {measurement}")
