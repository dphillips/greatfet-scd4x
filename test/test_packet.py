import packet
import pytest


def test_sensirion_crc8():
    # example from datasheet:
    assert packet.sensirion_crc8(b'\xBE\xEF') == 0x92
    # real world example: a serial number packet from the wild
    assert packet.sensirion_crc8(b'\xF0\xC1') == 0xEF
    assert packet.sensirion_crc8(b'\xBF\x07') == 0x1E
    assert packet.sensirion_crc8(b'\x3B\x2A') == 0x74


def test_sensirion_crc8_check_packet():
    test_packets = [
        # example from datasheet:
        (b'\xBE\xEF\x92', True),
        (b'\xBE\xEF\x93', False),
        # real world example: a serial number packet from the wild
        (b'\xF0\xC1\xEF\xBF\x07\x1E\x3B\x2A\x74', True),
        (b'\xF0\xC1\xEF\xBF\x17\x1E\x3B\x2A\x74', False),
        # edge case
        (b'', True),
    ]
    for raw_packet, is_valid in test_packets:
        assert is_valid == packet.sensirion_crc8_check_packet(raw_packet)

    for pattern in [b'\x12', '\x12\x34']:
        with pytest.raises(ValueError) as e:
            assert packet.sensirion_crc8_check_packet(pattern)
        assert "multiple of three" in str(e.value)


def test_sensirion_unpack_packet():
    test_packets = [
        # example from datasheet:
        (b'\xBE\xEF\x92', [0xBEEF]),
        # real world example: a serial number packet from the wild
        (b'\xF0\xC1\xEF\xBF\x07\x1E\x3B\x2A\x74',
            [0xF0C1, 0xBF07, 0x3B2A])
    ]
    for raw_packet, expected in test_packets:
        assert packet.sensirion_unpack_packet(raw_packet) == expected

    with pytest.raises(ValueError) as e:
        assert packet.sensirion_unpack_packet([0x12, 0x34, 0x56])
    assert "invalid CRC" in str(e.value)
