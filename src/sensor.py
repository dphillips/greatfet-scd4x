import collections
import packet

ScdCommand = collections.namedtuple(
    "ScdCommand",
    [
        "bytes",
        "execution_time_ms",
        "during_measurement",
        "response_length",
    ],
)


class _commands:
    # Basic commands
    start_periodic_measurement = ScdCommand(b"\x21\xb1", -1, False, 0)
    stop_periodic_measurement = ScdCommand(b"\x3f\x86", 500, True, 0)
    read_measurement = ScdCommand(b"\xec\x05", 1, True, 9)

    # On-chip output signal compensation
    set_temperature_offset = ScdCommand(b"\x24\x1d", 1, False, 0)
    get_temperature_offset = ScdCommand(b"\x23\x18", 1, False, 0)
    set_ambient_pressure = ScdCommand(b"\xe0\x00", 1, True, 0)
    set_sensor_altitude = ScdCommand(b"\x24\x27", 1, False, 0)
    get_sensor_altitude = ScdCommand(b"\x23\x22", 1, False, 0)

    # Field calibration
    set_automatic_self_calibration_enabled = ScdCommand(
        b"\x24\x16", 1, False, 0
    )
    get_automatic_self_calibration_enabled = ScdCommand(
        b"\x23\x13", 1, False, 0
    )
    perform_forced_recalibration = ScdCommand(b"\x36\x2f", 400, False, 0)

    # Low power
    start_low_power_periodic_measurement = ScdCommand(
        b"\x21\xac", -1, False, 0
    )
    get_data_ready_status = ScdCommand(b"\xe4\xb8", 1, True, 0)

    # Advanced features
    perform_factory_reset = ScdCommand(b"\x36\x32", 1200, False, 0)
    perform_self_test = ScdCommand(b"\x36\x39", 10000, False, 0)
    persist_settings = ScdCommand(b"\x36\x15", 800, False, 0)
    get_serial_number = ScdCommand(b"\x36\x82", 1, False, 9)
    reinit = ScdCommand(b"\x36\x46", 20, False, 0)

    # Low power single shot (SCD41 only)
    measure_single_shot_rht_only = ScdCommand(b"\x21\x96", 50, False, 0)
    measure_single_shot = ScdCommand(b"\x21\x9d", 5000, False, 0)


class Sensor:
    def __init__(self, driver):
        self._driver = driver

    def _execute(self, command):
        self._driver.send_bytes(command.bytes)
        if command.response_length:
            data = self._driver.recv_bytes(command.response_length)
            print(f"raw: {data}")
            response = packet.sensirion_unpack_packet(data)
        else:
            response = None

        return response

    def read_measurement(self):
        data = self._execute(_commands.read_measurement)
        co2_ppm, temp_raw, humid_raw = data
        temp_celsius = 175 * (temp_raw / 2**16) - 45
        humid_percent = 100 * (humid_raw / 2**16)
        return (co2_ppm, temp_celsius, humid_percent)

    def read_serial_number(self):
        serial = self._execute(_commands.get_serial_number)
        return f"0x{serial[0]:04x}{serial[1]:04x}{serial[2]:04x}"

    def measure_single_shot(self):
        self._execute(_commands.measure_single_shot)
