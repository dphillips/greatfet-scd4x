def sensirion_crc8(data):
    """SCD4x appear to use a custom CRC8, implemented here from datasheet."""
    crc8_polynomial = 0x31
    crc = 0xFF
    for data_byte in data:
        crc ^= data_byte
        for _ in range(8):
            if (crc & 0x80):
                crc = (crc << 1) ^ crc8_polynomial
            else:
                crc = (crc << 1)
    return crc & 0xFF


def sensirion_crc8_check_packet(packet):
    """Check validity of CRC8 bytes from a `bytes` Sensirion SCD4x packet

    This method assumes every third byte is a CRC8 checking the previous two,
    as is the case in Sensirion SCD4x packets"""
    packet_len = len(packet)
    if packet_len % 3 != 0:
        raise ValueError(f"packet size {packet_len} not a multiple of three")

    for i in range(packet_len//3):
        a, b, packet_crc8 = packet[i*3:i*3+3]
        crc8 = sensirion_crc8([a, b])
        packet_crc8_int = int(packet_crc8)
        if crc8 != packet_crc8_int:
            return False
    # fallthrough
    return True


def sensirion_unpack_packet(packet):
    if not sensirion_crc8_check_packet(packet):
        raise ValueError("packet contains invalid CRC")

    # packet is valid, drop every 3rd byte (crc) and merge adjacent of rest
    stripped = []
    for i in range(len(packet)//3):
        word = packet[i*3:i*3+2]
        stripped.append(int.from_bytes(word, 'big'))
    return stripped
