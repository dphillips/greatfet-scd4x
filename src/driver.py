class Scd41():
    # FIXME this class looks a lot like a simple stream. Can we use something
    # like that instead?

    def __init__(self):
        pass

    def send_bytes(self, data):
        raise NotImplementedError()

    def recv_bytes(self, bytes_count):
        raise NotImplementedError()
